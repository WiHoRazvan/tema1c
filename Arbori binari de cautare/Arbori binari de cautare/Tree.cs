﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arbori_binari_de_cautare
{
    class Tree
    {
        Node root = null;
        public void Add(int value)
        {
            if(root == null)
            {
                root = new Node();
                root.Value = value;
            }
            else
            {
                bool ready = true;
                Node aux = root;
                while(ready)
                {
                   if(value > aux.Value && aux.right==null)
                    {
                        aux.right = new Node();
                        aux.right.Value = value;
                        ready = false;
                        break;
                    }
                   else if(value > aux.Value && aux.right != null)
                    {
                        aux = aux.right;
                    }
                   else if(value <= aux.Value && aux.left == null)
                    {
                        aux.left = new Node();
                        aux.left.Value = value;
                        ready = false;
                        break;
                    } else if(value > aux.Value && aux.left != null)
                    {
                        aux = aux.left;
                    }
                   
                }
                
            }
        }
    }
}
