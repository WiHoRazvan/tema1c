﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arbori_binari_de_cautare
{
    class Node
    {
        public int Value { get; set; }
        public Node left = null;
        public Node right = null;
        public Node()
        {

        }

        
    }
}
