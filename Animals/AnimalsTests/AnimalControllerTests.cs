﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Animals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
namespace Animals.Tests
{
    [TestClass()]
    public class AnimalControllerTests
    {
        [TestMethod()]
        public void GetData_Test()
        {
            Mock<IAnimalRepository> repoMock = new Mock<IAnimalRepository>();
            Animal p1 = new Animal
            {
                FirstName = "John",
                LastName = "Smith",
                Id = 1
            };
            repoMock.Setup(x => x.Add(p1));

            repoMock.Setup(x => x.GetById(1)).Returns(p1);
            AnimalController controller = new AnimalController(repoMock.Object);

            Animal result = controller.GetById(1);


            Assert.AreEqual(p1.Id, result.Id);
            Assert.AreEqual(p1.Name, result.Name);
            Assert.AreEqual(p1.Description, result.Description);
        }
    }
}