﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    class Program
    {
        static void Main(string[] args)
        {
            AnimalRepository animalRepository = new AnimalRepository();
            AnimalController animalController = new AnimalController(animalRepository);
            Animal Lion = new Animal
            {
                Id = 1,
                Name = "Lion",
                Description = "King of the jungle"
            };

            Animal Bear = new Animal
            {
                Id = 2,
                Name = "Bear",
                Description = "Tracksuit with 3 stripes he wears"
            };

            animalController.Add(Lion);
            animalController.Add(Bear);
            

            while(true)
            {

            }
        }
    }
}
