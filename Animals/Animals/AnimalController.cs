﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public class AnimalController
    {
        readonly IAnimalRepository _animalRepository;

        public AnimalController(IAnimalRepository animalRepository)
        {
            _animalRepository = animalRepository;
        }

        public AnimalController()
        {
            _animalRepository = new AnimalRepository();
        }

        public void Add(Animal animal)
        {
            _animalRepository.Add(animal);
        }

        public void Delete(int index)
        {
            _animalRepository.Delete(index);
        }

        public void Show(int id)
        {
            _animalRepository.GetById(id);
        }

        public void Edit(int index)
        {
            _animalRepository.Edit(index);
        }

        public Animal GetById(int id)
        {
            return _animalRepository.GetById(id);
        }

        public IEnumerable<Animal> getAll() => _animalRepository.GetAll();
    }
}
