﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public interface IAnimalRepository
    {
     void Add(Animal animal);
     void Delete(int index);
     Animal GetById(int id);
        void Edit(int index);

        IEnumerable<Animal> GetAll();
    }
}
