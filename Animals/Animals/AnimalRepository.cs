﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    public class AnimalRepository:IAnimalRepository
    {
        public List<Animal> Animals { get; set; } = new List<Animal>();
        public void Add(Animal animal)
        {
            Animals.Add(animal);
        }
        public void Delete(int index)
        {
            foreach (var Animal in Animals)
            {
                if (Animal.Id == index)
                {
                    Animals.Remove(Animal);
                    break;
                }
            }
        }
        public Animal GetById(int id)
        {
            return Animals.SingleOrDefault(x => x.Id == id);// lambda structure
        }
        public void Edit(int index)
        {
            foreach (var Animal in Animals)
            {
                if(Animal.Id == index)
                {
                    Console.WriteLine("Enter animal name: ");
                    Animal.Name = Console.ReadLine();
                    Console.WriteLine("Enter animal description: ");
                    Animal.Description = Console.ReadLine();
                    
                }
            }
        }

        public IEnumerable<Animal> GetAll() => Animals;
    }
}
