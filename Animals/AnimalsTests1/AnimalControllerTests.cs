﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Animals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace Animals.Tests
{
    [TestClass()]
    public class AnimalControllerTests
    {
        [TestMethod()]
        public void GetData_Test()
        {
            Mock<IAnimalRepository> repoMock = new Mock<IAnimalRepository>();
            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Koala",
                Description = "Bear"
            };
            repoMock.Setup(x => x.Add(a1));

            repoMock.Setup(x => x.GetById(1)).Returns(a1);
            AnimalController controller = new AnimalController(repoMock.Object);

            Animal result = controller.GetById(1);


            Assert.AreEqual(a1.Id, result.Id);
            Assert.AreEqual(a1.Name, result.Name);
            Assert.AreEqual(a1.Description, result.Description);
        }


        public void GetAllData_Test()
        {
            Mock<IAnimalRepository> mock = new Mock<IAnimalRepository>();

            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Koala",
                Description = "Bear"
            };

            Animal a2 = new Animal
            {
                Id = 2,
                Name = "Panda",
                Description = "Bear"
            };

            IEnumerable<Animal> people = new List<Animal>
            {
                a1,
                a2
            };

            mock.Setup(x => x.GetAll()).Returns(people);

            AnimalController controller = new AnimalController(mock.Object);

            List<Animal> result = controller.getAll().ToList();

            Assert.AreEqual(2, result.Count);

        }
    }

}
