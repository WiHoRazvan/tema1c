﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Animals;

namespace AnimalsTests1
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
      
        
      

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            Animal a1 = new Animal
            {
                Id = 1,
                Name = "Koala",
                Description = "Bear"
                
            };

            Animal a2= new Animal
            {
                Id = 2,
                Name = "Panda",
                Description = "Bear"

            };

            AnimalRepository repo = new AnimalRepository();

            repo.Add(a1);
            repo.Add(a2);

            Animal first = repo.GetById(1);
            Animal second = repo.GetById(2);

            Assert.AreEqual("Koala", first.Name);
            Assert.AreEqual("Bear", first.Description);
            Assert.AreEqual(a1.Id, first.Id);
        }
    }
}
