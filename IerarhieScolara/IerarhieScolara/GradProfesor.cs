﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IerarhieScolara
{
    public enum GradProfesor
    {
        DEFINITIVAT,
        GRAD_2,
        GRAD_1,
        DOCTORAT
    }
}
