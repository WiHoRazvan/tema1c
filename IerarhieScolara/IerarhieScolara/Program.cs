﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IerarhieScolara
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Persoana> persoanas = new List<Persoana>();
            Persoana Alexandru = new Persoana
            {
                Nume = "Alexandru",
                Prenume = "Sorin",
                Adresa = "Strada Sorin nr 6"
            };

            Student Sorin = new Student
            {
                Nume = "Sorin",
                Prenume = "Sorinescu",
                Adresa = "Strada Sorineala numar 3 ",
                NrMatricol = "4431",
                Medie = 6.21
            };


            Profesor Camtarescu = new Profesor
            {
                Nume = "Camatarescu",
                Prenume = "Sorin",
                gradProfesor = GradProfesor.DEFINITIVAT
            };

            persoanas.Add(Alexandru);
            persoanas.Add(Sorin);
            persoanas.Add(Camtarescu);

            foreach (var Current in persoanas)
            {
                Console.WriteLine(Current.Nume);
            }

            persoanas.Sort((p, q) => p.Nume.CompareTo(q.Nume));
            Console.WriteLine("==============================");
            foreach (var Current in persoanas)
            {
                Console.WriteLine(Current.Nume);
            }

            while (true)
            {

            }
        }
    }
}
