﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Calculator
    {
        public double FirstOperand { get; set; }
        public double SecondOperand { get; set; }
        public double Result { get; set; }
        public String Operator { get; set; }
        public Calculator()
        {

        }
        public void MakeOperation()
        {
            Console.WriteLine("Insert the first number");
            FirstOperand = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert the second number");
            SecondOperand = double.Parse(Console.ReadLine());
            Console.WriteLine("Insert the operation");
            Operator = Console.ReadLine();
            switch (Operator)
            {
                case "+":
                    Result = FirstOperand + SecondOperand;
                    ShowResult(this.Result);
                    break;
                case "-":
                    Result = FirstOperand - SecondOperand;
                    ShowResult(this.Result);
                    break;
                case "*":
                    Result = FirstOperand * SecondOperand;
                    ShowResult(this.Result);
                    break;
                case "/":
                    if (SecondOperand == 0)
                        throw new ArithmeticException();
                    Result = FirstOperand / SecondOperand;
                    ShowResult(this.Result);
                    break;
                default:
                    Console.WriteLine("Not an operation");
                    throw new Exception("Wrong input");
                    break;
            }

            Console.WriteLine("New operation? Type Y/N");
            string a = Console.ReadLine();
            if(a=="Y" || a == "y")
            {
                this.MakeOperation();
            } else if(a == "N" || a == "n")
            {
                Console.WriteLine("See you soon");
            } else
            {
                Console.WriteLine("Wrong input, restart the app");
            }
        }

        public void ShowResult(Double result)
        {
            Console.WriteLine("The result is " + Result);
        }



    }
}
